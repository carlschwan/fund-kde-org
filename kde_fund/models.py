# SPDX-FileCopyrightText: (C) 2020 Carl Schwan <carl@carlschwan.eu>
#
# SPDX-LicenseRef: AGPL-3.0-or-later
from django.db import models


class PaypalDonation(models.Model):
    """
    Represent a individual Paypal donation
    """
    date = models.DateField()
    payment_amount = models.FloatField()
    message = models.CharField(max_length=255)
    transactionid = models.CharField(max_length=255, unique=True)
    donate_url = models.CharField(max_length=255)
