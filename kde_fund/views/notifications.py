# SPDX-FileCopyrightText: (C) 2020 Carl Schwan <carl@carlschwan.eu>
#
# SPDX-LicenseRef: AGPL-3.0-or-later

import datetime
import sys
import urllib.parse
import requests
from django.http import HttpResponse, HttpRequest
from django.views.generic.base import View
from ..models import PaypalDonation


class PaypalNotifyIPN(View):
    """
    Get payment notification from paypal
    """
    http_method_names = ['post']

    def post(self, request: HttpRequest, *args, **kwargs):

        VERIFY_URL_PROD = 'https://ipnpb.paypal.com/cgi-bin/webscr'
        VERIFY_URL_TEST = 'https://ipnpb.sandbox.paypal.com/cgi-bin/webscr'

        # Switch as appropriate
        VERIFY_URL = VERIFY_URL_TEST

        # Read and parse query string
        param_str = sys.stdin.readline().strip()
        params = urllib.parse.parse_qsl(param_str)

        # Add '_notify-validate' parameter
        params.append(('cmd', '_notify-validate'))

        # Post back to PayPal for validation

        headers = {'content-type': 'application/x-www-form-urlencoded',
                   'user-agent': 'Python-IPN-Verification-Script'}
        r = requests.post(VERIFY_URL, params=params, headers=headers, verify=True)
        r.raise_for_status()

        # Check return message and take action as needed
        if r.text == 'VERIFIED':
            item_name = request.POST['item_name']
            item_number = request.POST['item_number']
            payment_status = request.POST['payment_status']
            payment_amount = request.POST['mc_gross']
            payment_currency = request.POST['mc_currency']
            txn_id = request.POST['txn_id']
            memo = request.POST['memo']
            receiver_email = request.POST['receiver_email']
            payer_email = request.POST['payer_email']
            donate_url = request.POST['custom']
            try:
                date = datetime.datetime.strptime(request.POST["payment_date"], '%H:%M:% %b %d, %Y PDT')
            except ValueError:
                date = datetime.datetime.now()

            if payment_status != "Completed":
                return HttpResponse("Payment status is " + payment_status, status=500)

            if receiver_email != "kde-ev-board@kde.org":
                return HttpResponse("Unexpected receiver email: " + receiver_email)

            if payment_currency != "EUR":
                return HttpResponse("Unexpected currency:" + payment_currency)

            if PaypalDonation.objects.filter(txn_id=txn_id).count() > 0:
                donation = PaypalDonation.objects.get(txn_id=txn_id)
                donation.date = date
                donation.message = memo
                donation.donate_url = donate_url
                donation.payment_amount = payment_amount
                donation.txn_id = txn_id
                donation.save()
            else:
                PaypalDonation.objects.create(date=date, message=memo, donate_url=donate_url,
                                              payment_amount=payment_amount, txn_id=txn_id)
            return HttpResponse(status=200)
        return HttpResponse("NOT VERIFIED", status=500)
