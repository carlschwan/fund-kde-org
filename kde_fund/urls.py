from django.urls import path, reverse_lazy
from django.views.generic import RedirectView

from .views import notifications

urlpatterns = [
    path('notify', notifications.PaypalNotifyIPN.as_view(), name="notification"),
    path('', RedirectView.as_view(url=reverse_lazy('oauth:login')))
]
