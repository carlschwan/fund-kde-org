# SPDX-FileCopyrightText: (C) 2020 Carl Schwan <carl@carlschwan.eu>
#
# SPDX-LicenseRef: AGPL-3.0-or-later

import datetime
import sys
import urllib.parse
import requests
from django.http import HttpResponse, HttpRequest
from django.views.generic.base import View
from .models import PaypalDonation


